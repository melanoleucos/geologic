import wx
import xml.etree.ElementTree as ET
import os.path
import datetime
import wx.adv
import sys
import calendar
from pathlib import Path
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle


def panel_button(event):
    frame.update_panel(panel_name=event.GetEventObject().Name)


def _wxdate2pydate(date):

    assert isinstance(date, wx.DateTime)
    if date.IsValid():
        ymd = map(int, date.FormatISODate().split('-'))
        return datetime.date(*ymd)
    else:
        return None


class PanelOne(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)

        vbox = wx.BoxSizer(wx.VERTICAL)

        # Create two buttons.
        self.entry_button = wx.Button(self, -1, "Καταχώρηση", name="entry_button")
        self.entry_button.Bind(wx.EVT_BUTTON, panel_button)

        self.print_button = wx.Button(self, -1, "Εκτύπωση", name="print_button")
        self.print_button.Bind(wx.EVT_BUTTON, panel_button)

        # Add the buttons to the Sizer.
        vbox.Add(self.entry_button, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.print_button, 0, wx.ALL | wx.ALIGN_CENTER)

        self.SetSizer(vbox)


class PanelTwo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)

        vbox = wx.BoxSizer(wx.VERTICAL)

        self.colorRadioBox = wx.RadioBox(self, label="Επιλογή είδους", choices=["Έσοδο", 'Έξοδο'])
        self.amountText = wx.StaticText(self, label="Ποσό")
        self.amount = wx.TextCtrl(self, size=(150, -1))
        self.descriptionText = wx.StaticText(self, label="Σχόλια")
        self.description = wx.TextCtrl(self, size=(200, 100), style=wx.TE_MULTILINE)
        self.add_button = wx.Button(self, -1, "Αποθήκευση", name="save_entry_button")
        self.home_screen_btn = wx.Button(self, -1, "Αρχική οθόνη", name="home_button")

        vbox.AddSpacer(15)
        vbox.Add(self.amountText, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.amount, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.AddSpacer(5)
        vbox.Add(self.descriptionText, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.description, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.AddSpacer(5)
        vbox.Add(self.colorRadioBox, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.AddSpacer(5)
        vbox.Add(self.add_button, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.AddSpacer(5)
        vbox.Add(self.home_screen_btn, 0, wx.ALL | wx.ALIGN_CENTRE | wx.ID_HOME)

        self.add_button.Bind(wx.EVT_BUTTON, self.save_entry)
        self.home_screen_btn.Bind(wx.EVT_BUTTON, panel_button)

        self.SetSizer(vbox)

    def save_entry(self, event):
        amount_ts = self.amount.GetValue() or None
        description_ts = self.description.GetValue() or "-"
        radio_choice = self.colorRadioBox.GetSelection()

        if amount_ts is None:
            pass
        if description_ts is None:
            pass

        type_ts = None
        if radio_choice == 0:
            type_ts = "income"
        elif radio_choice == 1:
            type_ts = "outcome"

        date_ts = datetime.datetime.now().strftime("%Y-%m-%d")

        home_string = str(os.path.dirname(os.path.realpath(sys.executable)))  # __file__

        valid_xml_input = False

        if self.is_number(amount_ts):
            valid_xml_input = True

        if valid_xml_input:
            # Confirm form entry.
            dlg = wx.MessageDialog(self, "Είστε σίγουρος για τα παραπάνω στοιχεία;", "Eπιβεβαίωση", wx.YES_NO | wx.ICON_QUESTION)
            entry_confirmed = dlg.ShowModal() == wx.ID_YES
            dlg.Destroy()

            if entry_confirmed:
                if os.path.exists(home_string + "\\test.xml"):
                    tree = ET.parse(home_string + "\\test.xml")
                    root = tree.getroot()
                    doc = ET.SubElement(root, "entry", type=type_ts)

                    ET.SubElement(doc, "amount").text = amount_ts
                    ET.SubElement(doc, "description").text = description_ts
                    ET.SubElement(doc, "date").text = date_ts

                    tree.write("test.xml")
                else:
                    root = ET.Element("entries")
                    doc = ET.SubElement(root, "entry", type=type_ts)

                    ET.SubElement(doc, "amount").text = amount_ts
                    ET.SubElement(doc, "description").text = description_ts
                    ET.SubElement(doc, "date").text = date_ts

                    tree = ET.ElementTree(root)
                    tree.write("test.xml")

                # Reset the form.
                self.colorRadioBox.SetSelection(0)
                self.amount.Clear()
                self.description.Clear()
        else:
                dlg = wx.MessageDialog(self, "Το χρηματικό ποσό πρέπει να είναι αριθμός(π.χ. 3.14)."
                                             "\nΓια τα δεκαδικά ψηφία χρησιμοποιείται το σύμβολο της τελέιας(.)",
                                       "Λάθος δεδομένα", wx.OK | wx.ICON_WARNING)
                dlg.ShowModal()
                dlg.Destroy()

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False


class PanelThree(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)

        vbox = wx.BoxSizer(wx.VERTICAL)

        self.report_type_radiobox = wx.RadioBox(self, label="Επιλογή είδους", choices=["Αναφορά ημέρας",
                                                                                       "Αναφορά εβδομάδας",
                                                                                       "Αναφορά μήνα",
                                                                                       "Αναφορά έτους",
                                                                                       "Αναφορά 'Από...εώς'"])
        self.start_date = wx.adv.DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        self.end_date = wx.adv.DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        self.report_print_button = wx.Button(self, -1, "Εκτύπωση αναφοράς", name="print_report_button")
        self.home_screen_btn = wx.Button(self, -1, "Αρχική οθόνη", name="home_button")

        vbox.Add(self.report_type_radiobox, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.start_date, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.end_date, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.Add(self.report_print_button, 0, wx.ALL | wx.ALIGN_CENTRE)
        vbox.AddSpacer(5)
        vbox.Add(self.home_screen_btn, 0, wx.ALL | wx.ALIGN_CENTRE | wx.ID_HOME)

        self.report_print_button.Bind(wx.EVT_BUTTON, self.query_data)
        self.home_screen_btn.Bind(wx.EVT_BUTTON, panel_button)

        self.SetSizer(vbox)

    def query_data(self, event):
        genesis = None

        report_type_selection = self.report_type_radiobox.GetSelection()
        exodus = datetime.datetime.now().date()

        if report_type_selection == 0:
            # Day report.
            genesis = exodus - datetime.timedelta(days=0)
        elif report_type_selection == 1:
            # Week report.
            genesis = exodus - datetime.timedelta(days=6)
        elif report_type_selection == 2:
            # Month report.
            genesis = exodus - datetime.timedelta(days=30)
        elif report_type_selection == 3:
            # Year report.
            genesis = exodus - datetime.timedelta(days=365)
        elif report_type_selection == 4:
            # Hand-picked dates report.
            genesis = _wxdate2pydate(self.start_date.GetValue())
            exodus = _wxdate2pydate(self.end_date.GetValue())

            # Check if starting date is before or equal the end date.
            if genesis > exodus:
                return

        incomes = self.load_rows("i", report_type_selection, genesis, exodus)
        outcomes = self.load_rows("o", report_type_selection, genesis, exodus)

        total_sum = 0
        total_loss = 0

        for entry in incomes:
            total_sum += float(entry[0])

        for entry in outcomes:
            total_loss += float(entry[0])

        doc = SimpleDocTemplate(os.path.join(os.environ["HOMEPATH"], "") + "Αναφορά.pdf", pagesize=A4,
                                rightMargin=30, leftMargin=30, topMargin=30, bottomMargin=18)
        doc.pagesize = landscape(A4)
        elements = []

        style = ParagraphStyle(
            name="yolo"
        )

        # TODO: Get this line right instead of just copying it from the docs
        t_style = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                              ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                              ('VALIGN', (0, 0), (0, -1), 'TOP'),
                              ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                              ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                              ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                              ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                              ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                              ('BOX', (0, 0), (-1, -1), 0.25, colors.black)])

        data = incomes

        if data:
            elements.append(Paragraph('ΕΣΟΔΑ', style))

            column1_heading = Paragraph("<para align=center>ΠΟΣΟ</para>", style)
            column2_heading = Paragraph("<para align=center>ΣΧΟΛΙΑ</para>", style)
            columnd3_heading = Paragraph("<para align=center>ΗΜΕΡΟΜΗΝΙΑ</para>", style)
            row_array = [column1_heading, column2_heading, columnd3_heading]
            table_heading = [row_array]
            th = Table(table_heading)
            th.setStyle(t_style)
            elements.append(th)

            # Configure style and word wrap
            s = getSampleStyleSheet()
            s = s["BodyText"]
            s.wordWrap = 'CJK'
            data2 = [[Paragraph(cell, s) for cell in row] for row in data]
            t = Table(data2)
            t.setStyle(t_style)

            # Send the data and build the file
            elements.append(t)

        data = outcomes

        if data:
            elements.append(Paragraph('<br />' + 'ΕΞΟΔΑ', style))

            column1_heading = Paragraph("<para align=center>ΠΟΣΟ</para>", style)
            column2_heading = Paragraph("<para align=center>ΣΧΟΛΙΑ</para>", style)
            columnd3_heading = Paragraph("<para align=center>ΗΜΕΡΟΜΗΝΙΑ</para>", style)
            row_array = [column1_heading, column2_heading, columnd3_heading]
            table_heading = [row_array]
            th = Table(table_heading)
            th.setStyle(t_style)
            elements.append(th)

            # Configure style and word wrap
            s = getSampleStyleSheet()
            s = s["BodyText"]
            s.wordWrap = 'CJK'
            data2 = [[Paragraph(cell, s) for cell in row] for row in data]
            t = Table(data2)
            t.setStyle(t_style)

            # Send the data and build the file
            elements.append(t)

        elements.append(Paragraph('<br />' + 'ΣΥΝΟΛΙΚΑ ΕΣΟΔΑ: ' + str(total_sum), style))
        elements.append(Paragraph('ΣYΝΟΛΙΚΑ ΕΞΟΔΑ: ' + str(total_loss), style))

        dif = total_sum - total_loss
        if total_sum <= total_loss:
            word_used = "ΖΗΜΙΑ: "
        else:
            word_used = "ΚΕΡΔΟΣ: "

        elements.append(Paragraph(word_used + str(abs(dif)), style))
        doc.build(elements)

    def monthdelta(self, date, delta):
        m, y = (date.month+delta) % 12, date.year + (date.month+delta-1) // 12

        if not m: m = 12

        d = min(date.day, [31, 29 if (y % 4 == 0) and not (y % 400 == 0) else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m-1])

        return date.replace(day=d, month=m, year=y)

    def get_month_day_range(self, date):
        first_day = date.replace(day=1)
        last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
        return first_day, last_day

    def load_rows(self, array_type, report_type, genesis, exodus):
        data = None
        incomes = []
        outcomes = []
        sum1 = 0
        sum2 = 0
        sum3 = 0
        sum4 = 0
        sum5 = 0
        loss_sum1 = 0
        loss_sum2 = 0
        loss_sum3 = 0
        loss_sum4 = 0
        loss_sum5 = 0

        xml = ET.parse(str(os.path.dirname(os.path.realpath(sys.executable))) + "\\test.xml")  # __file__
        root = xml.getroot()

        if report_type == 0 or report_type == 1 or report_type == 4:
            for i, entry in enumerate(root.findall("entry")):
                entry_type = entry.get("type")
                string_entry_date = entry.find("date").text
                entry_date = datetime.datetime.strptime(string_entry_date, "%Y-%m-%d").date()

                if entry_type == "income" and (genesis <= entry_date <= exodus):
                    entry_row = []

                    for feed in entry:
                        entry_row.append(feed.text)
                    incomes.append(entry_row)
                elif entry_type == "outcome" and (genesis <= entry_date <= exodus):
                    entry_row = []
                    for feed in entry:
                        entry_row.append(feed.text)
                    outcomes.append(entry_row)
        elif report_type == 2:
            week5_end = datetime.datetime.now().date()

            week1_start = self.monthdelta(week5_end, -1)
            week1_end = week1_start + datetime.timedelta(days=6)

            week2_start = week1_end + datetime.timedelta(days=1)
            week2_end = week2_start + datetime.timedelta(days=6)

            week3_start = week2_end + datetime.timedelta(days=1)
            week3_end = week3_start + datetime.timedelta(days=6)

            week4_start = week3_end + datetime.timedelta(days=1)
            week4_end = week4_start + datetime.timedelta(days=6)

            week5_start = week4_end + datetime.timedelta(days=1)

            for i, entry in enumerate(root.findall("entry")):
                entry_type = entry.get("type")
                string_entry_date = entry.find("date").text
                entry_date = datetime.datetime.strptime(string_entry_date, "%Y-%m-%d").date()

                entry_amount = float(entry.find("amount").text)

                if entry_type == "income":
                    if week5_start <= entry_date <= week5_end:
                        sum5 += entry_amount
                    elif week4_start <= entry_date <= week4_end:
                        sum4 += entry_amount
                    elif week3_start <= entry_date <= week3_end:
                        sum3 += entry_amount
                    elif week2_start <= entry_date <= week2_end:
                        sum2 += entry_amount
                    elif week1_start <= entry_date <= week1_end:
                        sum1 += entry_amount
                elif entry_type == "outcome":
                    if week5_start <= entry_date <= week5_end:
                        loss_sum5 += entry_amount
                    elif week4_start <= entry_date <= week4_end:
                        loss_sum4 += entry_amount
                    elif week3_start <= entry_date <= week3_end:
                        loss_sum3 += entry_amount
                    elif week2_start <= entry_date <= week2_end:
                        loss_sum2 += entry_amount
                    elif week1_start <= entry_date <= week1_end:
                        loss_sum1 += entry_amount

            incomes.append([str(sum1), "-", "1η ΕΒΔΟΜΑΔΑ (" + week1_start.strftime("%d/%m/%Y") + " - " + week1_end.strftime("%d/%m/%Y") + ")"])
            incomes.append([str(sum2), "-", "2η ΕΒΔΟΜΑΔΑ (" + week2_start.strftime("%d/%m/%Y") + " - " + week2_end.strftime("%d/%m/%Y") + ")"])
            incomes.append([str(sum3), "-", "3η ΕΒΔΟΜΑΔΑ (" + week3_start.strftime("%d/%m/%Y") + " - " + week3_end.strftime("%d/%m/%Y") + ")"])
            incomes.append([str(sum4), "-", "4η ΕΒΔΟΜΑΔΑ (" + week4_start.strftime("%d/%m/%Y") + " - " + week4_end.strftime("%d/%m/%Y") + ")"])
            incomes.append([str(sum5), "-", "5η ΕΒΔΟΜΑΔΑ (" + week5_start.strftime("%d/%m/%Y") + " - " + week5_end.strftime("%d/%m/%Y") + ")"])

            outcomes.append([str(loss_sum1), "-", "1η ΕΒΔΟΜΑΔΑ (" + week1_start.strftime("%d/%m/%Y") + " - " + week1_end.strftime("%d/%m/%Y") + ")"])
            outcomes.append([str(loss_sum2), "-", "2η ΕΒΔΟΜΑΔΑ (" + week2_start.strftime("%d/%m/%Y") + " - " + week2_end.strftime("%d/%m/%Y") + ")"])
            outcomes.append([str(loss_sum3), "-", "3η ΕΒΔΟΜΑΔΑ (" + week3_start.strftime("%d/%m/%Y") + " - " + week3_end.strftime("%d/%m/%Y") + ")"])
            outcomes.append([str(loss_sum4), "-", "4η ΕΒΔΟΜΑΔΑ (" + week4_start.strftime("%d/%m/%Y") + " - " + week4_end.strftime("%d/%m/%Y") + ")"])
            outcomes.append([str(loss_sum5), "-", "5η ΕΒΔΟΜΑΔΑ (" + week5_start.strftime("%d/%m/%Y") + " - " + week5_end.strftime("%d/%m/%Y") + ")"])
        elif report_type == 3:
            month12_start = datetime.datetime.now().date()
            initial_year_number = month12_start.strftime("%Y")
            months_start_end = []

            counter = 0
            while True:
                year_number = self.monthdelta(month12_start, -counter).strftime("%Y")
                if year_number == initial_year_number:
                    months_start_end.append(self.get_month_day_range(self.monthdelta(month12_start, -counter)))

                    counter += 1
                else:
                    break

        if array_type == "i":
            data = incomes
        elif array_type == "o":
            data = outcomes

        return data


class MyForm(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Logistics", size=wx.Size(800, 400))

        self.panel_one = PanelOne(self)
        self.panel_two = PanelTwo(self)
        self.panel_three = PanelThree(self)

        self.panel_two.Hide()
        self.panel_three.Hide()

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.panel_one, 1, wx.EXPAND)
        self.sizer.Add(self.panel_two, 1, wx.EXPAND)
        self.sizer.Add(self.panel_three, 1, wx.EXPAND)

        self.SetSizer(self.sizer)

    def update_panel(self, panel_name):
        if panel_name == "entry_button":
            self.panel_one.Hide()
            self.panel_three.Hide()
            self.panel_two.Show()
        elif panel_name == "print_button":
            self.panel_one.Hide()
            self.panel_two.Hide()
            self.panel_three.Show()
        elif panel_name == "home_button":
            self.panel_two.Hide()
            self.panel_three.Hide()
            self.panel_one.Show()

        self.Layout()


# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()
